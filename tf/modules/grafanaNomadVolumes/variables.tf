variable "plugin_id" {
  type = string
  description = "The Nomad plugin ID to wait for"
}

variable "cifs_user_id" {
  type = number
  description = "The User ID to specify for the mount options"
}

variable "cifs_group_id" {
  type = number
  description = "The Group ID to specify for the mount options"
}
