[server]
protocol = http
http_port = 3000
domain = grafana.soc.carboncollins.se
root_url = https://%(domain)s/

[security]
disable_initial_admin_creation = true

[users]
allow_sign_up = false
allow_org_create = false

auto_assign_org = true
auto_assign_org_id = 1
auto_assign_org_role = Viewer

[auth]
disable_login_form = true
oauth_auto_login = false
oauth_allow_insecure_email_lookup = true

[auth.basic]
enabled = false

[auth.generic_oauth]
enabled = true
name = CarbonCollins Cloud
allow_sign_up = true
allowed_domains = carboncollins.se,carboncollins.uk
auto_login = false

{{ with secret "identity/oidc/client/grafana" }}
client_id = {{ index .Data.client_id }}
client_secret = {{ index .Data.client_secret }}
{{ end }}

scopes = openid,email,profile,c3-groups
email_attribute_name = email
email_attribute_path = email
login_attribute_path = preferred_username
name_attribute_path = name
role_attribute_path = contains("https://carboncollins.se/groups"[*], 'grafana-admin') && 'Admin' || contains("https://carboncollins.se/groups"[*], 'grafana-editor') && 'Editor' || 'Viewer'
;role_attribute_strict = false
;groups_attribute_path = 
;team_ids_attribute_path =

skip_org_role_sync = true
allow_assign_grafana_admin = true

auth_url = https://vault.soc.carboncollins.se/ui/vault/identity/oidc/provider/c3/authorize
token_url = https://vault.soc.carboncollins.se/v1/identity/oidc/provider/c3/token
api_url = https://vault.soc.carboncollins.se/v1/identity/oidc/provider/c3/userinfo

[log]
mode = console
level = info

[unified_alerting]
enabled = false
; This is disabled for now

[alerting]
enabled = false

[metrics]
enabled = true
interval_seconds = 10

[metrics.environment_info]

[tracing.opentelemetry]

[date_formats]
use_browser_locale = true
default_week_start = browser
default_timezone = "Europe/Stockholm"
